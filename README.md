# Contador de Ensaios CCB

Essa aplicação foi desenvolvida com o intuito de sistematizar a contagem de músicos, cidades
e irmandades de um ensaio local ou regional da congregação cristã no Brasil. 

### Congregação Cristã no Brasil
Apesar da aplicação ser um auxílio para os membros da Congregação Cristã no Brasil, esta não possui nenhum vínculo com a igreja.