CREATE DATABASE IF NOT EXISTS event_count;

USE event_count;

CREATE TABLE IF NOT EXISTS state (
  code int unsigned primary key auto_increment,
  name varchar(200),
  uf varchar(2) not null,
  ibge int(2) DEFAULT NULL,
  country int(3) DEFAULT NULL,
  ddd varchar(50) DEFAULT NULL,
  createdat datetime default current_timestamp,
  editedat datetime default null,
  enabled boolean default true
) DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;

CREATE TABLE IF NOT EXISTS city (
  code int unsigned primary key auto_increment,
  name varchar(200),
  state int unsigned not null,
  ibge int(7) DEFAULT NULL,
  createdat datetime default current_timestamp,
  editedat datetime default null,
  enabled boolean default true
) DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;

CREATE TABLE IF NOT EXISTS order_city (
  code int unsigned primary key auto_increment,
  position int unsigned default 1,
  city int unsigned not null,
  event int unsigned not null,
  createdat datetime default current_timestamp,
  editedat datetime default null,
  enabled boolean default true
);

CREATE TABLE IF NOT EXISTS event (
  code int unsigned primary key auto_increment,
  name varchar(200),
  access_key varchar(10) not null,
  city int unsigned default null,
  cities_to_use_church longtext default  '[]',
  eventdate date default null,
  status enum ('created', 'finished') default 'created',
  createdat datetime default current_timestamp,
  editedat datetime default null,
  enabled boolean default true
) DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;

CREATE TABLE IF NOT EXISTS role (
  code int unsigned primary key auto_increment,
  name varchar(200),
  createdat datetime default current_timestamp,
  editedat datetime default null,
  enabled boolean default true
) DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;


CREATE TABLE IF NOT EXISTS instrument (
  code int unsigned primary key auto_increment,
  name varchar(200),
  category enum('strings', 'wood', 'brass', 'other'),
  createdat datetime default current_timestamp,
  editedat datetime default null,
  enabled boolean default true
) DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;

CREATE TABLE IF NOT EXISTS church (
  code int unsigned primary key auto_increment,
  name varchar(200),
  city int unsigned not null,
  createdat datetime default current_timestamp,
  editedat datetime default null,
  enabled boolean default true
) DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;

CREATE TABLE IF NOT EXISTS instrument_count (
  code int unsigned primary key auto_increment,
  event int unsigned not null,
  instrument int unsigned not null,
  quantity int default 0,
  createdat datetime default current_timestamp,
  editedat datetime default null,
  enabled boolean default true
);

CREATE TABLE IF NOT EXISTS role_count (
  code int unsigned primary key auto_increment,
  event int unsigned not null,
  role int unsigned not null,
  quantity int default 0,
  createdat datetime default current_timestamp,
  editedat datetime default null,
  enabled boolean default true
);

CREATE TABLE IF NOT EXISTS city_count (
  code int unsigned primary key auto_increment,
  event int unsigned not null,
  city int unsigned not null,
  church int unsigned default null,
  quantity int default 0,
  createdat datetime default current_timestamp,
  editedat datetime default null,
  enabled boolean default true
);


DELIMITER $
BEGIN NOT ATOMIC
    IF (SELECT COUNT(*) FROM state) = 0 THEN 
      BEGIN

        INSERT INTO state (code, name, uf, ibge, country, ddd) VALUES
        (1, 'Acre', 'AC', 12, 1, '68'),
        (2, 'Alagoas', 'AL', 27, 1, '82'),
        (3, 'Amazonas', 'AM', 13, 1, '97,92'),
        (4, 'Amapá', 'AP', 16, 1, '96'),
        (5, 'Bahia', 'BA', 29, 1, '77,75,73,74,71'),
        (6, 'Ceará', 'CE', 23, 1, '88,85'),
        (7, 'Distrito Federal', 'DF', 53, 1, '61'),
        (8, 'Espírito Santo', 'ES', 32, 1, '28,27'),
        (9, 'Goiás', 'GO', 52, 1, '62,64,61'),
        (10, 'Maranhão', 'MA', 21, 1, '99,98'),
        (11, 'Minas Gerais', 'MG', 31, 1, '34,37,31,33,35,38,32'),
        (12, 'Mato Grosso do Sul', 'MS', 50, 1, '67'),
        (13, 'Mato Grosso', 'MT', 51, 1, '65,66'),
        (14, 'Pará', 'PA', 15, 1, '91,94,93'),
        (15, 'Paraíba', 'PB', 25, 1, '83'),
        (16, 'Pernambuco', 'PE', 26, 1, '81,87'),
        (17, 'Piauí', 'PI', 22, 1, '89,86'),
        (18, 'Paraná', 'PR', 41, 1, '43,41,42,44,45,46'),
        (19, 'Rio de Janeiro', 'RJ', 33, 1, '24,22,21'),
        (20, 'Rio Grande do Norte', 'RN', 24, 1, '84'),
        (21, 'Rondônia', 'RO', 11, 1, '69'),
        (22, 'Roraima', 'RR', 14, 1, '95'),
        (23, 'Rio Grande do Sul', 'RS', 43, 1, '53,54,55,51'),
        (24, 'Santa Catarina', 'SC', 42, 1, '47,48,49'),
        (25, 'Sergipe', 'SE', 28, 1, '79'),
        (26, 'São Paulo', 'SP', 35, 1, '11,12,13,14,15,16,17,18,19'),
        (27, 'Tocantins', 'TO', 17, 1, '63');
        
      END;
    END IF;

    IF (SELECT COUNT(*) FROM event) = 0 THEN 
      BEGIN

        INSERT INTO event(name, city, access_key, eventdate, cities_to_use_church) VALUES 
          ("Ensaio Regional de Presidente Prudente", 
          5172, "12345", "2022-08-07", "[5172]");
        
      END;
    END IF;

    IF (SELECT COUNT(*) FROM role) = 0 THEN 
      BEGIN

        INSERT INTO role(name) VALUES 
          ("Ancião"), ("Diácono"), ("Cooperador de Adulto"),
          ("Cooperador de Jovens"), ("Encarregado Regional"), 
          ("Encarregado Local"), ("Organista Examinadora"),
          ("Organista"), ("Irmandade");
        
      END;
    END IF;

    IF (SELECT COUNT(*) FROM instrument) = 0 THEN 
      BEGIN

        INSERT INTO instrument(name, category) VALUES 
          ("Violino", "strings"),        
          ("Viola", "strings"),        
          ("Violoncelo", "strings"),   

          ("Flauta Transversal", "wood"),        
          ("Oboé", "wood"),        
          ("Oboé D' Amore", "wood"),        
          ("Corne Inglês", "wood"),        
          ("Fagote", "wood"),        
          ("Clarinete", "wood"),        
          ("Clarinete Alto", "wood"),        
          ("Clarone", "wood"),        
          ("Saxofone Soprano", "wood"),        
          ("Saxofone Alto", "wood"),        
          ("Saxofone Tenor", "wood"),        
          ("Saxofone Barítono", "wood"),        
          ("Saxofone Baixo", "wood"),        

          ("Trompete", "brass"),        
          ("Cornet", "brass"),        
          ("Flugelhorn", "brass"),        
          ("Trompa", "brass"),        
          ("Trombone", "brass"),        
          ("Trombonito", "brass"),        
          ("Eufônio", "brass"),        
          ("Barítono", "brass"),        
          ("Tuba", "brass"),

          ("Acordeon", "other"),        
          ("Outro", "other");

      END;
    END IF;
END; $