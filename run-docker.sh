commandname="docker compose";
mode="development";

if [ ! -z "$1" -a "$1" != " " ]
then 
    commandname="$1";
fi

$commandname -f docker-compose.yml up -d --build database;
docker exec database_count sh /database/autoload.sh;

$commandname -f docker-compose.yml up -d --build frontend;

if [ "$mode" != "development" ]
then 
    $commandname -f docker-compose.yml up -d --build backend;
else
    $commandname -f docker-compose.yml up --build backend;
fi