const { Buffer } = require('buffer');
const separator = '$#$(';

function sentResponse(result, data, message, req, res, code = 200){
    return res.status(code).send({
        result, data, message, 
        'method': req.method,
        'url': req.originalUrl, 
        'params': req.params,
        'body': req.body,
        'query': req.query,
    });
}

function validationFields(needed, fields){
    return new Promise((resolve, reject) => {
        if(needed.every(need => {
            if(!fields.includes(need))
                return reject(`Não foi possível encontrar o campo (${need})`);
            return true;
        })) resolve(true);
    });
}

function authorization(req, res, next){
    try {
        const jwt = require('jsonwebtoken');
        const token = (req.headers.authorization ? 
            (req.headers.authorization.split(' ').length > 1 ? req.headers.authorization.split(' ')[1] : ''):
            ''
        );
        var authDataEncode = jwt.verify(token, process.env.JWT_KEY);


        if(authDataEncode){
            if(authDataEncode && authDataEncode.code)
                var authData = decode(authDataEncode.code);
                if(authData)
                    authData = JSON.parse(authData);
        }
            
        if(authData && authData.event && authData.event.code){
            var eventcode = decode(authData.event.code);

            
            if(eventcode){
                const repositoryEvent = require('./../repository/event');
               
                repositoryEvent.listByCode(eventcode).then(event => {
                    if(!event) throw "Contador de Ensaio não encontrado!";
                    global.event = event;
                    next();
                }).catch(error => sentResponse(false, error, 'Falha na Autenticação', req, res));

            } else throw "Contador de Ensaio não encontrado!";
        } else throw "Não é possível continuar com a requsição";
    } catch(err) {
        sentResponse(false, null, 'Falha na autenticação', req, res, 401);
    }
}

function constructQuery(type, table, datas, conditional = { '1': { value: 1, operator: '='}}, operator = "AND", acceptNull = false){
    if(!acceptNull) var fields = Object.keys(datas).filter(field => datas[field] !== null);
    else var fields = Object.keys(datas);

    var conditionalFields = Object.keys(conditional);
    var queryString = '';
    var params = [];
    if(type == "select"){
        queryString = `select ${datas} from ${table} 
        WHERE ${conditionalFields.map(field => `${field} ${conditional[field].operator} ?`).join(` ${operator} `)}`;
        params = conditionalFields.map(field => conditional[field].value);
    } else if(type == "update"){
        queryString = `update ${table} set ${fields.map(field => `${field} = ?`)} WHERE ${conditionalFields.map(field => `${field} ${conditional[field].operator} ?`).join(` ${operator} `)}`;
        params = fields.map(field => datas[field]).concat(conditionalFields.map(field => conditional[field].value));
    } else if(type == "insert"){
        queryString = `insert into ${table}(${fields}) values (${fields.map(() => '?')})`;
        params = params.concat(fields.map(field => datas[field]));
    }

    return { queryString, params };
}

function filterObject(object, fields){
    var _clone = object;
    var keys = Object.keys(_clone);
    keys.forEach(key => {
        if(!fields.includes(key)) delete _clone[key];

        if(_clone[key] === '') _clone[key] = null;
    });
    return _clone;
}

function rollback(error, conn, reject){
    conn.rollback(() => {
        conn.release();
        reject(error);
    });
}

function commit(result, conn, resolve){
    conn.commit(() => {
        conn.release();
        resolve(result);
    });
}

function encode(text){
    return Buffer.from(
        Buffer.from(String(text)).toString('base64') + separator + Buffer.from(String(Date.now())).toString('base64') 
    ).toString('base64');
}

function decode(hash){
    var part1 = Buffer.from(hash, 'base64').toString('utf-8');
    var [part2, part3] = part1.split(separator);
    if(!part3) return false;
    return Buffer.from(part2, 'base64').toString('utf-8');
}

function generatePassword(length = 12){
    var chars = "0123456789abcdefghijklmnopqrstuvwxyz!@#$%^&*()ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var password = "";
    for (var index = 0; index <= length; index++) {
        var randomNumber = Math.floor(Math.random() * chars.length);
        password += chars.substring(randomNumber, randomNumber +1);
    }
    return password;
}

function generateAccessKey(length = 5){
    var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var accessKey = "";
    for (var index = 0; index <= length; index++) {
        var randomNumber = Math.floor(Math.random() * chars.length);
        accessKey += chars.substring(randomNumber, randomNumber +1);
    }
    return accessKey;
}

function buildConditions(values, fields, globalOperator = false){
    var conditions = [];
    var params = [];
    Object.keys(values).forEach(key => {
        if(fields[key] && fields[key].restrictions && !fields[key].restrictions.includes(values[key])) {
            conditions.push(`${fields[key].value} ${fields[key].operator} ?`);
            params.push(values[key]);
        }
    });

    return {
        conditions: (globalOperator ? conditions.join(` ${globalOperator} `) : conditions),
        params
    }
}

function getMonthNameByNumber(month){
    if(!month || typeof month !== "number") return "-";

    var months = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho",
    "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];
    return months[month - 1];
}

module.exports = {
    sentResponse,
    validationFields,
    authorization,
    constructQuery,
    filterObject,
    rollback,
    commit,
    encode,
    decode,
    generatePassword,
    generateAccessKey,
    buildConditions,
    getMonthNameByNumber
}