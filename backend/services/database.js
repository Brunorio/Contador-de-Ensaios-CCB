const { pool } = require('../mysql');

function typeQuery(queryString){
    var splitted = queryString.split(' ');
    if(splitted.length > 0)
        return splitted[0].toLocaleLowerCase();
    return false;
}

function __execute(conn, queryString, params = [], releaseConnection = true){
    return new Promise((resolve, reject) => {
        conn.query(queryString, params, function(error, results) {
            if(releaseConnection)
                conn.release();

            if(!results || error)
                return reject(error);
            
            var type = typeQuery(queryString);
            if(type == "insert") return resolve(results.insertId);
            else if(type == "update")
                if(results.affectedRows > 0) return resolve(results.affectedRows);
                else return reject('Nenhuma linha afetada');
                
            return resolve(results);                
        });
    })
}

function query(queryString, params = [], __connection = false, manager = false){
    return new Promise(function(resolve, reject){
        if(__connection) __execute(__connection, queryString, params, false).then(resolve).catch(reject);
        else {
            pool.getConnection(function(errorConn, conn){
                if(!conn)
                    return reject(errorConn);
                
                if(errorConn){
                    conn.release();
                    return reject(errorConn);
                }

                __execute(conn, queryString, params).then(resolve).catch(reject);
            });
        }
    });
}

function connection(){
    return new Promise(function(resolve, reject){
        pool.getConnection(function(errorConn, conn){
            if(!conn)
                return reject(errorConn);
       
            if(errorConn){
                conn.release();
                return reject(errorConn);
            }
            resolve(conn);
        });
    });
}

module.exports = {
    query,
    connection
};