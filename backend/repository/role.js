const { query } = require('../services/database'); 

function listByCode(code){
    return new Promise((resolve, reject) => {
        query('select * from role where enabled is true and code = ?', [code])
        .then(results => results.length > 0 ? resolve(results[0]) : reject('Registro não encontrado!'))
        .catch(reject);
    });
}

function listEnabled(){
    return new Promise((resolve, reject) => {
         query(`
         select role.*,
         coalesce((select sum(quantity) from role_count where role_count.role = role.code and role_count.event = ?), 0) as quantity
         from role 
         where enabled is true
         `, [ global.event.code || 0 ]).then(resolve).catch(reject);
    });
}

function insertQuantity({ code, quantity }){
    return new Promise((resolve, reject) => {
        query(`insert into role_count (role, quantity, event) values (?, ?, ?)`, [code, quantity, global.event.code])
        .then(resolve).catch(reject);
    });
}




module.exports = {
    listByCode, listEnabled,
    insertQuantity
}