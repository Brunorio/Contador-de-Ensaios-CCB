const { query } = require('../services/database'); 
const { constructQuery, filterObject } = require('../services/handle');

function listByCode(code){
    return new Promise((resolve, reject) => {
        query('select * from event where enabled is true and code = ?', [code])
        .then(results => results.length > 0 ? resolve(results[0]) : reject('Registro não encontrado!'))
        .catch(reject);
    });
}

function listByAccessKey(accessKey){
    return new Promise((resolve, reject) => {
        query('select * from event where enabled is true and access_key = ?', [accessKey])
        .then(results => results.length > 0 ? resolve(results[0]) : reject('Registro não encontrado!'))
        .catch(reject);
    });
}

function listEnabled(){
    return new Promise((resolve, reject) => {
         query('select * from event where enabled is true').then(resolve).catch(reject);
    });
}

function enabledToDisabled(code){
    return new Promise((resolve, reject) => {
        query('update event set enabled = false, editedat = ? where code = ? and enabled is true', [Date.now(), code])
        .then(resolve).catch(reject);
    });
}

function finish(){
    return new Promise((resolve, reject) => {
        query(`update event set status = 'finished' where code = ?`, [global.event.code])
        .then(resolve).catch(reject);
    });
}

function create(event){
    return new Promise((resolve, reject) => {
        var eventFiltered = filterObject({...event}, [
            'name', 'eventdate', 'access_key'
        ]);

        const { queryString, params } = constructQuery('insert', 'event', eventFiltered);
        query(queryString, params).then(resolve).catch(reject);
    })

}

module.exports = {
    create,
    listByCode, listEnabled, listByAccessKey,
    enabledToDisabled, finish
}