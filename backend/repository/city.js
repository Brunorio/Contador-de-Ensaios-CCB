const { query } = require('../services/database'); 
const { filterObject, constructQuery } = require('../services/handle');

function listByOrderCity(){
    return new Promise((resolve, reject) => {
        query(`select city.code, city.name, 
        concat(city.name, ' - ', state.uf) namestate,
        state.code statecode,
        state.name state,
        state.uf,
        (select coalesce(sum(quantity), 0) from city_count where city_count.event = ? and
            city_count.city = city.code and city_count.enabled = 1) quantity
        from city
        join state on state.code = city.state
        join order_city on order_city.city = city.code
        join event on event.code = order_city.event
        where order_city.event = ? and order_city.enabled = 1
        order by city.name, order_city.position`, [global.event.code || 0, global.event.code || 0]).then(resolve).catch(reject);
    });
}

function listByCode(code){
    return new Promise((resolve, reject) => {
        query('select * from city where enabled is true and code = ?', [code])
        .then(results => results.length > 0 ? resolve(results[0]) : reject('Registro não encontrado!'))
        .catch(reject);
    });
}

function listEnabled(){
    return new Promise((resolve, reject) => {
         query('select * from city where enabled is true').then(resolve).catch(reject);
    });
}

function listByState(state){
    return new Promise((resolve, reject) => {
         query('select * from city where enabled is true and state = ?', [state]).then(resolve).catch(reject);
    });
}

function listByState(state){
    return new Promise((resolve, reject) => {
        query(`select distinct
        city.code,
        city.name,
        city.state,
        if(order_city.code, 1, 0) isenabled
        from city
        left join order_city on order_city.city = city.code and order_city.event = ? 
        where city.state = ? 
        order by city.name`, [global.event.code, state]).then(resolve).catch(reject);
    });
}

function enabledToDisabled(code){
    return new Promise((resolve, reject) => {
        query('update city set enabled = false, editedat = ? where code = ? and enabled is true', [Date.now(), code])
        .then(resolve).catch(reject);
    });
}

function insertQuantity(data){
    return new Promise((resolve, reject) => {
        var dataFiltered = filterObject({...data}, [
            'city', 'quantity', 'church'
        ]);
        dataFiltered['event'] = global.event.code;
        var { queryString, params } = constructQuery('insert', 'city_count', dataFiltered); 
        query(queryString, params).then(resolve).catch(reject);

    });
}

function listMaxPositionOfOrderCity(){
    return new Promise((resolve, reject) => {
        query(`select coalesce(max(position), 0) max from order_city where event = ?`, 
        [global.event.code]).then(results => results.length ? resolve(results[0].max) : resolve(0))
        .catch(reject);
    });
}

function listCityOfOrderCity(citycode){
    return new Promise((resolve, reject) => {
        query(`select * from order_city where city = ? and event = ?`, 
        [citycode, global.event.code])
        .then(results => results.length ? resolve(results[0]) : resolve(null))
        .catch(reject);
    });
}

function createOrderCity(citycode, position){
    return new Promise((resolve, reject) => {
        query(`insert into order_city(city, position, event) values(?, ?, ?)`, 
        [citycode, position, global.event.code]).then(resolve).catch(reject);
        insertQuantity({
            city: citycode,
            quantity: 1
        });
    });
}

module.exports = {
    listByCode, listEnabled, listByOrderCity,
    listByState,
    listMaxPositionOfOrderCity,
    listCityOfOrderCity,
    enabledToDisabled,
    insertQuantity,
    createOrderCity
}