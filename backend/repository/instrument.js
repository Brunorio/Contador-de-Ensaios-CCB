const { query } = require('../services/database'); 

function listByCode(code){
    return new Promise((resolve, reject) => {
        query('select * from instrument where enabled is true and code = ?', [code])
        .then(results => results.length > 0 ? resolve(results[0]) : reject('Registro não encontrado!'))
        .catch(reject);
    });
}

function listEnabled(){
    return new Promise((resolve, reject) => {
         query(`
         select instrument.*,
         coalesce((select sum(quantity) from instrument_count where instrument_count.instrument = instrument.code and instrument_count.event = ?), 0) as quantity
         from instrument 
         where enabled is true
         `, [ global.event.code || 0 ]).then(resolve).catch(reject);
    });
}

function insertQuantity({ code, quantity }){
    return new Promise((resolve, reject) => {
        query(`insert into instrument_count (instrument, quantity, event) values (?, ?, ?)`, [code, quantity, global.event.code])
        .then(resolve).catch(reject);
    });
}




module.exports = {
    listByCode, listEnabled,
    insertQuantity
}