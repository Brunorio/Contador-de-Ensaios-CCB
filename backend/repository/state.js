const { query } = require('../services/database'); 

function listByCode(code){
    return new Promise((resolve, reject) => {
        query('select * from state where enabled is true and code = ?', [code])
        .then(results => results.length > 0 ? resolve(results[0]) : reject('Registro não encontrado!'))
        .catch(reject);
    });
}

function listEnabled(){
    return new Promise((resolve, reject) => {
         query('select * from state where enabled is true').then(resolve).catch(reject);
    });
}

function enabledToDisabled(code){
    return new Promise((resolve, reject) => {
        query('update state set enabled = false, editedat = ? where code = ? and enabled is true', [Date.now(), code])
        .then(resolve).catch(reject);
    });
}



module.exports = {
    listByCode, listEnabled,
    enabledToDisabled
}