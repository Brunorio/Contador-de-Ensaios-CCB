const { query } = require('../services/database'); 

function listByCode(code){
    return new Promise((resolve, reject) => {
        query('select * from church where enabled is true and code = ?', [code])
        .then(results => results.length > 0 ? resolve(results[0]) : reject('Registro não encontrado!'))
        .catch(reject);
    });
}

function listEnabled(){
    return new Promise((resolve, reject) => {
         query('select * from church where enabled is true').then(resolve).catch(reject);
    });
}

function listEnabledAndByCity(city){
    return new Promise((resolve, reject) => {
        query(`select *,
        coalesce((select sum(city_count.quantity) from city_count where city_count.enabled = 1 
        and city_count.church = church.code and city_count.event = ?), 0) quantity
        from church 
        where enabled is true and city = ? order by church.name`, [global.event.code, city]).then(resolve).catch(reject);
    });
}

function enabledToDisabled(code){
    return new Promise((resolve, reject) => {
        query('update church set enabled = false, editedat = ? where code = ? and enabled is true', [Date.now(), code])
        .then(resolve).catch(reject);
    });
}



module.exports = {
    listByCode, listEnabled,
    listEnabledAndByCity,
    enabledToDisabled
}