const repository = require('./../../repository/role');
const { sentResponse } = require('../../services/handle');

function byEnabled(req, res){
   repository.listEnabled().then(results => sentResponse(true, results, 'Registros listados com sucesso!', req, res))
        .catch(message => sentResponse(false, null, message, req, res));
}

function byCode(req, res){
   if(req.params.code || false) repository.listByCode(req.params.code).then(result => sentResponse(true, result, 'Registro listado com sucesso!', req, res))
        .catch(message => sentResponse(false, null, message, req, res));
   else return sentResponse(false, null, 'Não foi possível encontrar o registro!', req, res);
}

module.exports = {
   byEnabled,
   byCode
}