const repository = require('./../../repository/role');
const { sentResponse, validationFields } = require('../../services/handle');

function byCodeAndQuantity(req, res){
    if(global.event.status !== 'created') return sentResponse(false, null, 'Contador de Ensaio finalizado!', req, res);

    validationFields(["code", "quantity"], Object.keys(req.body)).then(async () => {
        try {
            var role = repository.listByCode(req.body.code);
            if(!role) throw 'Role not found!';
        } catch(err){
            console.log(err);
            return sentResponse(false, null, 'Não foi possível encontrar o cargo');
        }

        repository.insertQuantity(req.body).then(result => sentResponse(true, result, 'Registro criado com sucesso!', req, res))
        .catch(message => sentResponse(false, null, message, req, res));
   }).catch(message => sentResponse(false, null, message, req, res));
}



module.exports = {
    byCodeAndQuantity
}