const repository = require('./../../repository/event');
const { sentResponse, validationFields, generateAccessKey } = require('../../services/handle');

function byFields(req, res){
   validationFields(['name'], Object.keys(req.body)).then(async () => {
    let accessKey;
      try {
        do {
          accessKey = generateAccessKey(5);
          let exists = await repository.listByAccessKey(accessKey);
        } while(exists);
      } catch(e){
        let body = {...req.body, access_key: accessKey};
        repository.create(body).then(result => sentResponse(true, {...result, accessKey}, 'Registro criado com sucesso!', req, res))
          .catch(message => sentResponse(false, null, message, req, res));
      }

      
   }).catch(message => sentResponse(false, null, message, req, res));
}

module.exports = {
   byFields
}