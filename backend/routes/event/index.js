const express = require("express");
const router = express.Router();
const repository = require('./../../repository/event');
const { sentResponse } = require('../../services/handle');

router.get('/finish', (req, res) => {
    repository.finish().then(() => sentResponse(true, null, 'Contador de Ensaio finalizado', req, res))
    .catch(message => {
        console.log(message);
        sentResponse(false, null, message, req, res);
    });
});

module.exports = router;