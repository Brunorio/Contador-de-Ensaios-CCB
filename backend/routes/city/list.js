const repository = require('./../../repository/city');
const repositoryState = require('./../../repository/state');
const { sentResponse } = require('../../services/handle');

function byOrderCity(req, res){
   repository.listByOrderCity().then(results => sentResponse(true, results, 'Registros listados com sucesso!', req, res))
        .catch(message => sentResponse(false, null, message, req, res));
}

function byCode(req, res){
   if(req.params.code || false) repository.listByCode(req.params.code).then(result => sentResponse(true, result, 'Registro listado com sucesso!', req, res))
        .catch(message => sentResponse(false, null, message, req, res));
   else return sentResponse(false, null, 'Não foi possível encontrar o registro!', req, res);
}

function byState(req, res){
   if(req.params.state || false) repository.listByState(req.params.state).then(async results => {
      try {
         return sentResponse(true, {
            cities: results,
            state: await repositoryState.listByCode(req.params.state)
         }, 'Registro listado com sucesso!', req, res)
      } catch(err) {
         console.log(err);
         return sentResponse(false, null, 'Não foi possível listar os registros!', req, res);
      }
   }).catch(message => sentResponse(false, null, message, req, res));
   else return sentResponse(false, null, 'Não foi possível encontrar o registro!', req, res);
}


module.exports = {
   byOrderCity, byState,
   byCode
}