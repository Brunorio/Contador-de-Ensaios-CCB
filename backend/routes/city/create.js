const repository = require('./../../repository/city');
const { sentResponse, validationFields } = require('../../services/handle');

function byCodeAndQuantity(req, res){
    if(global.event.status !== 'created') return sentResponse(false, null, 'Contador de Ensaio finalizado!', req, res);

   validationFields(["city", "quantity"], Object.keys(req.body)).then(async () => {
        try {
            var city = await repository.listByCode(req.body.city);
            if(!city) throw 'City not found!';
            
        } catch(err){
            console.log(err);
            return sentResponse(false, null, 'Não foi possível encontrar a cidade');
        }

        repository.insertQuantity(req.body).then(result => sentResponse(true, result, 'Registro criado com sucesso!', req, res))
        .catch(message => sentResponse(false, null, message, req, res));
   }).catch(message => sentResponse(false, null, message, req, res));
}

function orderCity(req, res){
    if(global.event.status !== 'created') return sentResponse(false, null, 'Contador de Ensaio finalizado!', req, res);
    
    validationFields(["city"], Object.keys(req.body)).then(async () => {
         try {
            var city = await repository.listByCode(req.body.city);
            if(!city) throw 'City not found!';

            var event = await repository.listCityOfOrderCity(req.body.code);
            if(event) return sentResponse(true, null, 'Registro encontrado', req, res);

            var maxPosition = await repository.listMaxPositionOfOrderCity();
             
         } catch(err){
             console.log(err);
             return sentResponse(false, null, 'Não foi possível encontrar a cidade');
         }
 
         repository.createOrderCity(req.body.city, (maxPosition + 1)).then(result => sentResponse(true, result, 'Registro criado com sucesso!', req, res))
         .catch(message => sentResponse(false, null, message, req, res));
    }).catch(message => sentResponse(false, null, message, req, res));
 }
 


module.exports = {
    byCodeAndQuantity,
    orderCity
}