const express = require("express");
const router = express.Router();
const list = require('./list');
const create = require('./create');

// List
router.get('/list', list.byOrderCity);
router.get('/state/:state', list.byState);
router.get('/:code', list.byCode);

router.put('/insert', create.byCodeAndQuantity);
router.put('/include', create.orderCity);


module.exports = router;