const express = require("express");
const router = express.Router();
const create = require('./../event/create');

router.post('/', create.byFields);

module.exports = router;