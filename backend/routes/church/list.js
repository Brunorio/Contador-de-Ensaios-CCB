const repository = require('./../../repository/church');
const repositoryCity = require('./../../repository/city');
const { sentResponse } = require('../../services/handle');

function byEnabled(req, res){
   repository.listEnabled().then(results => sentResponse(true, results, 'Registros listados com sucesso!', req, res))
        .catch(message => sentResponse(false, null, message, req, res));
}

function byCity(req, res){
   if(req.params.city || false) repository.listEnabledAndByCity(req.params.city).then(async results => {
        
        try {
            var toSend = {
                churchs: results,
                city: await repositoryCity.listByCode(req.params.city)
            };
        } catch(e){
            console.log(e);
            return sentResponse(false, null, 'Não foi possível encontrar o registro!', req, res);
        }

        sentResponse(true, toSend, 'Registro listado com sucesso!', req, res)
   })
        .catch(message => sentResponse(false, null, message, req, res));
   else return sentResponse(false, null, 'Não foi possível encontrar o registro!', req, res);
}

module.exports = {
   byEnabled,
   byCity
}