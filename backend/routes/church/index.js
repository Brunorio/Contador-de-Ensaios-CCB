const express = require("express");
const router = express.Router();
const list = require('./list');

// List
router.get('/list', list.byEnabled);
router.get('/city/:city', list.byCity);

module.exports = router;