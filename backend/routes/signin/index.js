const express = require("express");
const jwt = require('jsonwebtoken');
const repository = require('../../repository/event');
const { validationFields, sentResponse, encode } = require('../../services/handle');

const router = express.Router();

router.post('/', function(req, res){
    
    validationFields(['accessKey'], Object.keys(req.body))
    .then(() => {
        
        repository.listByAccessKey(req.body.accessKey)
        .then(event => {
            if(!event) return sentResponse(false, null, 'Não foi possível realizar a autenticação', req, res, 401);

            let authData = {
                event: {
                    code: encode(event['code']),
                    name: event['name'],
                    citiesToUseChurch: JSON.parse(event['cities_to_use_church'] || "[]"),
                    finished: (event['status'] == 'finished'),
                    createdat: Date.now()
                }
            };

            try {
                let token = jwt.sign({ code: encode(JSON.stringify(authData))}, process.env.JWT_KEY, { expiresIn: "3h" });
                delete authData['code'];
                sentResponse(true, {token, ...authData}, "Contador de Ensaio encontrado com sucesso!", req, res);
            } catch(e){
                sentResponse(false, null, null, req, res, 400);
                console.log(e);
            }

        }).catch(message => sentResponse(false, null, message, req, res, 400));
    }).catch(message => sentResponse(false, null, message, req, res, 400));
});

module.exports = router;
