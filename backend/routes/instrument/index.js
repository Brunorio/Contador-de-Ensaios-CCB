const express = require("express");
const router = express.Router();
const list = require('./list');
const create = require('./create');

// List
router.get('/list', list.byEnabled);
router.get('/:code', list.byCode);

router.put('/insert', create.byCodeAndQuantity);


module.exports = router;