const express = require("express");
const app = express();
const morgan = require("morgan");
const bodyParser = require('body-parser');
const cors = require('cors');

global.event = {
    code: null,
    name: null
}

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Header', 
    'Content-type, Origin, X-Requested-With, Accept, Authorization');

    next();
});

const { sentResponse, authorization } = require('./services/handle');

try {
    app.use('/signin', require('./routes/signin'));
    app.use('/signup', require('./routes/signup'));
    app.use('/state', require('./routes/state'));

    app.use('/city', authorization, require('./routes/city'));
    app.use('/instrument', authorization, require('./routes/instrument'));
    app.use('/role', authorization, require('./routes/role'));
    app.use('/church', authorization, require('./routes/church'));
    app.use('/event', authorization, require('./routes/event'));

    app.use((error, req, res, next) => {
        return sentResponse(false, null, "Não foi possível encontrar a rota", req, res, 400);
    });
} catch(err){
    console.log(err);

    app.use((error, req, res, next) => {
        return sentResponse(false, null, "Problema com a requisição", req, res, 500);
    });
}



module.exports = app;