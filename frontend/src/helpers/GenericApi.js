import api from "../services/api";

function listItem(itemrequest, code = false){
    return new Promise((resolve, reject) => {
        api().get(`/${itemrequest}/${code || ''}`).then(response => {
            if(response.status == 200 && response.data.result)
                return resolve(response.data.data);

            return reject([]);
        }).then(error => reject([]));
    })
}

function saveItem(item, itemrequest, update = true){
    return new Promise((resolve, reject) => {
        api()[update ? 'put' : 'post'](`/${itemrequest}`, {...item}).then(response => {
            if(((update && response.status == 200) || (!update && response.status == 201)) 
            && response.data.result)
                return resolve(response.data.data);
            return reject(response.data.message);
        }).catch(({ response }) => {
            reject(response.data.message || false)
        });
    });
}

export {
    listItem,
    saveItem
}