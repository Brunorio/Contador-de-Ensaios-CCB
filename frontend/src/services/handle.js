const calculateDateInterval = (dateInitial, dateFinal) => {
    if(!dateFinal || !dateInitial) return '-';
    var timeInitial = (new Date(dateInitial)).getTime();
    var timeFinal = (new Date(dateFinal)).getTime();
    var interval = Math.abs((timeInitial - timeFinal)/1000);

    var inDays = (interval / 60) / 60 / 24;
    var years = Math.trunc(inDays / 365);
    var months = Math.trunc(inDays % 365 / 30);
    var days = Math.trunc(inDays % 365 % 30);

    var yearText = years ? `${years} ano${years > 1 ? 's' : ''}` : '';
    var monthText = months ? `${months} ${months === 1 ? 'mês' : 'meses'}` : '';
    var dayText = days ? `${days} dia${days > 1 ? 's' : ''}` : '';
    return  `${yearText} ${monthText} ${dayText}`;
}

var ALPHA = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
const encode = (value) => {
    
    if (typeof(value) !== 'number') return value;

    var result = '', mod;
    do {
        mod = value % 64;
        result = ALPHA.charAt(mod) + result;
        value = Math.floor(value / 64);
    } while(value > 0);

    return result;
};

const decode = (value) => {
    var result = 0;
    for (var i = 0, len = value.length; i < len; i++) {
        result *= 64;
        result += ALPHA.indexOf(value[i]);
    }
    return result;
};

module.exports = {
    calculateDateInterval,
    encode, decode
}