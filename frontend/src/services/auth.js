const AUTH_DATA = "@contador-Auth-Data";

function isAuthenticated() {
    try {
        const { isExpired, decodeToken } = require("react-jwt");
        var authData = getAuthData();
        if(authData && authData.token){
            var tokenDecoded = decodeToken(authData.token);
            return tokenDecoded.exp*1000 > Date.now();
        }
        return false;
    } catch(e){
        return false;
    }
    
}

function getAuthData(){
    var authData = localStorage.getItem(AUTH_DATA);
    if(authData && authData !== '') {
        try {
            return JSON.parse(authData);
        } catch(error){
            return false;
        }
    }
    return false;
}

function setAuthData(data){
    removeAuthData();
    if(typeof data === 'object') data = JSON.stringify(data);
    localStorage.setItem(AUTH_DATA, data);
}

function removeAuthData(){
    localStorage.removeItem(AUTH_DATA);
}

export {
    isAuthenticated,
    getAuthData,
    setAuthData,
    removeAuthData
}