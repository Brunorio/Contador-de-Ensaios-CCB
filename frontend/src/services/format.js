const formatCurrency = (value, currency = false, minimumFractionDigits = 0) => {
    var options = { style: 'decimal', minimumFractionDigits: minimumFractionDigits };
    if(currency) options = {...options, style: 'currency', currency: 'BRL', minimumFractionDigits: 2};
    return parseFloat(value || 0).toLocaleString('pt-BR', options);
}

const formatDate = (timestamp, type = 'full') => {
    var options = {};
    if(type === 'full') options = {
        minute: 'numeric', hour: 'numeric'
    }
    return new Date(timestamp).toLocaleDateString('pt-br', options);
}

const formatPhone = (number) => {
    if(!number) return number;
    
    var numberFormatted = number.replace(/\D/g, '');

    if(numberFormatted.indexOf('55') === 0)
        numberFormatted = numberFormatted.substring(2);

    if(numberFormatted.indexOf('0') === 0)
        numberFormatted = numberFormatted.substring(1);
    
    if(numberFormatted.length === 11){
        var position = numberFormatted.match(/(\d{2})(\d{5})(\d{4})/);
        return `(${position[1]}) ${position[2]} - ${position[3]}`;
    } else if(numberFormatted.length === 10){
        var position = numberFormatted.match(/(\d{2})(\d{4})(\d{4})/);
        return `(${position[1]}) ${position[2]} - ${position[3]}`;
    } else if(numberFormatted.length === 9){
        var position = numberFormatted.match(/(\d{5})(\d{4})/);
        return `${position[1]} - ${position[2]}`;
    } else if(numberFormatted.length === 8){
        var position = numberFormatted.match(/(\d{4})(\d{4})/);
        return `${position[1]} - ${position[2]}`;
    }
    return number;
}

const formatDocument = (number) => {
    if(!number) return number;

    var numberFormatted = number.replace(/\D/g, '');

    if(numberFormatted.length === 14) {
        var position = numberFormatted.match(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/);
        return `${position[1]}.${position[2]}.${position[3]}/${position[4]}-${position[5]}`;
    } else if(numberFormatted.length === 11) {
        var position = numberFormatted.match(/(\d{3})(\d{3})(\d{3})(\d{2})/);
        return `${position[1]}.${position[2]}.${position[3]}-${position[4]}`;
    }
    return number;
}

export {
    formatCurrency,
    formatDate,
    formatPhone,
    formatDocument
}