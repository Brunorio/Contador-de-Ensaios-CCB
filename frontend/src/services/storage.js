function getStorageItem(name){
    var item = localStorage.getItem(name);
    if(item && item !== '') {
        try {
            var converted = JSON.parse(item);
            return converted;
        } catch(error){
            return item;
        }
    }
    return undefined;
}

function removeStorageItem(name){
    localStorage.removeItem(name);
}

function setStorageItem(name, item){
    if(typeof item === 'object') item = JSON.stringify(item);
    localStorage.setItem(name, item);
}

export {
    getStorageItem,
    removeStorageItem,
    setStorageItem
}