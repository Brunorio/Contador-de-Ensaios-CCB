import { create } from 'axios';
import { isAuthenticated, getAuthData } from '../services/auth';

var dataApi = {
    api: null,
    token: null
};

export default function api(){
    const isAuthed = isAuthenticated();
    const token = isAuthed ? getAuthData().token : null;

    if(!(dataApi.api !== null && token === dataApi.token)) {
        var options = {
            baseURL: process.env.REACT_APP_API_URL,
            headers: {}
        }
        
        dataApi.token = token;
        
        if(isAuthed)
            options['headers']['Authorization'] = `Bearer ${token}`;
        
        dataApi = {
            api: create(options),
            token
        }  
    }
    
    return dataApi.api;
};