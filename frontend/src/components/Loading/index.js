import React from "react";

import styles from "./styles.module.css";

export default function Loading({ show, message }){

    if(!show) return <></>;

    return (
        <div className={styles.content}>
            <div>{message || 'Aguarde...'}</div>
        </div>
    )
}