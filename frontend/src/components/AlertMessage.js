import React from "react";
import Alert from 'react-bootstrap/Alert';

export default function AlertMessage(props){

    if(!props.show) return <></>;

    return (
        <Alert variant={props.variant} dismissible onClose={props.onClose || (() => {})}>
            {props.title ? (<Alert.Heading>{props.title}</Alert.Heading>) : <></>}
            <p>{props.message}</p>
        </Alert>
    )
}