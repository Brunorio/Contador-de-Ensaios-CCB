import React from "react";
import { useNavigate } from "react-router-dom";

import styles from "./styles.module.css";

export default function CounterChurchCard(props) {
    var navigate = useNavigate();
  
    const handleClick = () => {
        navigate(`/church/${props.code}`);
    }

    return (
        <>
            <div className={styles.content}>
                <div className={styles.title}>
                    <div>{props.title}</div>
                    <div className={styles.amount}>{props.quantity || 0}</div>
                </div>
                <div className={styles.valuefor}>
                    <div onClick={handleClick}>
                        <div>Listar Comuns</div>
                    </div>
                </div>
            </div>
        </>
        
    )
}