import React, { useState, useEffect } from "react";
import Form from 'react-bootstrap/Form';
import api from '../../services/api';

export default () => {
  const [ states, setStates ] = useState([]);

  const listState = () => {
    api().get('/state/list').then(response => {
        if(response.status == 200 && response.data.result && response.data.data)
            setStates(response.data.data);
    }).catch(err => {
        console.log(err);
    });
  }

  useEffect(() => {
    listState();
  }, []);

  return (
    <Form.Group className="mb-3" controlId="formBasicQuantity">
      <Form.Label>Estado</Form.Label>
      <Form.Select size="lg">
          {states.map(state => <option key={state.uf} value={state.code}>{state.name} - {state.uf}</option>)}
      </Form.Select>
    </Form.Group>
  )
}