import { useState } from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { solid } from "@fortawesome/fontawesome-svg-core/import.macro";

import AlertMessage from '../AlertMessage';
import api from '../../services/api';

export default (props) => {
  let { showModal } = props;
  var newEventMock = {
    name: '',
    eventdate: ''
  };

  const [ newEvent, setNewEvent ] = useState(newEventMock);
  const [ showAlert, setShowAlert ] = useState(false);
  const [ disabled, setDisabled ] = useState(false);
  const [ accessKey, setAccessKey ] = useState('');
  const [ textButtonCopy, setTextButtonCopy ] = useState(() => <span>Copiar <FontAwesomeIcon size="xs" icon={solid("copy")} /></span>)
  
  const handleClose = () => {
    setNewEvent(newEventMock);
    props.handleClose();
  }

  const handleChange = (val, key) => {
    setNewEvent({...newEvent, [key]: val });
  }

  const handleClickCopy = () => {
    navigator.clipboard.writeText(accessKey);
    setTextButtonCopy(() => <span style={{ color: 'green' }}>Copiado! <FontAwesomeIcon size="xs" icon={solid("check")} /></span> );

    setTimeout(() => {
      setTextButtonCopy(<span>Copiar <FontAwesomeIcon size="xs" icon={solid("copy")} /></span>);
    }, 8000);
  }

  const handleConfirm = () => {
    setDisabled(true);
    api().post('/signup', {...newEvent}).then(response => {
      if(response.status == 200 && response.data.result && response.data.data){
        setAccessKey(response.data.data.accessKey);
      } else setShowAlert(true);
    }).catch(err => {
      setShowAlert(true);
      console.log(err);
    }).finally(() => setDisabled(false));
  }

  const AccessKey = () => {
    if(accessKey)
      return (
        <div>
          <small>Esta é a sua chave para acessar o contador. Salve ela em algum lugar seguro.</small>
          <div style={{ display: 'flex', flexDirection: 'column', fontSize: 35, textAlign: 'center', fontWeight: 'bold' }}>
            {accessKey}
            <Button variant="link" onClick={handleClickCopy}>{textButtonCopy}</Button>
          </div>
          
        </div>
      )
  }

  return (
    <Modal show={showModal} onHide={handleClose}>
        <Modal.Header closeButton>
            <Modal.Title>Adicionar Nova Contagem de Ensaio</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            {!accessKey && <>
              <Form.Group className="mb-3" controlId="formBasicQuantity">
                  <Form.Label>Descrição</Form.Label>
                  <Form.Control size="lg"  value={newEvent.name} onChange={e => handleChange(e.target.value, 'name')}  placeholder="Ensaio de ..." 
                  />
              </Form.Group>
              <Form.Group className="mb-3" controlId="formBasicQuantity">
                  <Form.Label>Data</Form.Label>
                  <Form.Control size="lg" type="date" value={newEvent.eventdate} onChange={e => handleChange(e.target.value, 'eventdate')} placeholder="Data do ensaio ..." 
                  />
              </Form.Group>
              <AlertMessage show={showAlert} onClose={() => setShowAlert(false)} variant="danger" message="Não foi possível adicionar a nova contagem de ensaio."  />
            </>}
            <AccessKey />
        </Modal.Body>
        <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
                {!accessKey ? 'Cancelar' : 'Concluir'}
            </Button>
            {!accessKey && <Button disabled={disabled} onClick={handleConfirm} variant="primary" >
                Confirmar
            </Button>}
            
        </Modal.Footer>
    </Modal>
  );
}