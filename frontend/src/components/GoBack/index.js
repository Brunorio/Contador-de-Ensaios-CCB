import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { solid } from "@fortawesome/fontawesome-svg-core/import.macro";
import { useNavigate } from "react-router-dom";

import styles from './styles.module.css';

export default function GoBack(props){
    var navigate = useNavigate();

    return(
        <div className={styles.content}>
            <div className={styles.button} onClick={() => navigate(props.backTo || -1)}>
                <FontAwesomeIcon size="xl" icon={solid("arrow-left")} />
                <span>VOLTAR</span>
            </div>
            {props.title && (
                <div style={{ 
                    marginRight: props.hideSync !== true ? 0 : 10
                 }}>
                    <h3 className={styles.title}>{props.title}</h3>
                </div>
            )}
            {props.hideSync !== true && (
                <div className={styles.button} onClick={() => window.location.reload()  }>
                    <FontAwesomeIcon size="xl" icon={solid("rotate")} />
                    <span>SYNC</span>
                </div>
            )}
            
        </div>
    )

}