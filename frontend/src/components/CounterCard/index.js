import React, { useState } from "react";
import Modal from 'react-bootstrap/Modal';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { solid } from "@fortawesome/fontawesome-svg-core/import.macro";

import { getAuthData } from "../../services/auth";
import styles from "./styles.module.css";

export default function CounterCard(props) {
    const [show, setShow] = useState(false);
    const [quantity, setQuantity ] = useState("");

    const event = getAuthData().event;

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const handleConfirm = (_quantity) => {
        if(!_quantity) return;
        
        _quantity = parseInt(_quantity) || 0;

        var type = document.getElementById("inline-sub");
        if(type && type.checked)
            _quantity *= -1;

        if(props.handleValue && typeof props.handleValue == "function"){
            setQuantity("");
            handleClose();
            props.handleValue(_quantity);
        }
    }

    return (
        <>
            <div className={styles.content}>
                <div className={styles.title}>
                    <div>{props.title}</div>
                    <div className={styles.amount}>{props.quantity}</div>
                </div>
                {!event.finished && (
                    <div className={styles.valuefor}>
                        <div className={styles.operators}>
                            <div onClick={handleShow}>
                                <div>V</div>
                            </div>
                            <div onClick={() => {
                                    handleConfirm(-1);
                                }}>
                                <div><FontAwesomeIcon size="sm" icon={solid("minus")}  /></div>
                            </div>
                            <div onClick={() => {
                                    handleConfirm(1);
                                }}>
                                <div><FontAwesomeIcon size="sm" icon={solid("plus")} /></div>
                            </div>
                        </div>
                    </div>
                )}
            </div>
             <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>{props.title}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form.Group className="mb-3" controlId="formBasicQuantity">
                        <Form.Label>Quantidade</Form.Label>
                        <Form.Control size="lg" maxLength={3} value={quantity || ""} onChange={e => {
                            setQuantity(e.target.value || 0);
                        }} autoFocus type="number" inputMode="decimal" placeholder="Quantidade contada" 
                        />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicType">
                        <Form.Check
                            inline
                            label="Somar"
                            name="type"
                            type={"radio"}
                            defaultChecked
                            id={"inline-sum"}
                        />
                        <Form.Check
                            inline
                            label="Subtrair"
                            name="type"
                            type={"radio"}
                            id={"inline-sub"}
                        />

                    </Form.Group>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Cancelar
                    </Button>
                    <Button variant="primary" onClick={() => {
                        handleConfirm(quantity);
                    }}>
                        Confirmar
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
        
    )
}
