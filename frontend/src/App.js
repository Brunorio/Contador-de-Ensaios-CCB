import React, { useEffect, useState } from 'react';
import { Route, Routes, Navigate, BrowserRouter, Outlet } from 'react-router-dom';

import { isAuthenticated } from './services/auth';

import 'bootstrap/dist/css/bootstrap.min.css';

import SignIn from './pages/SignIn';
import Home from './pages/Home';
import Instrument from './pages/Instrument';
import Role from './pages/Role';
import Place from './pages/Place';
import City from './pages/City';
import Church from './pages/Church';
import State from './pages/State';
import Report from './pages/Report';

import NotFound from './pages/NotFound';

export default function App() {
  return (
        <BrowserRouter>
          <Routes>
            <Route path="/signin" element={<SignIn />} />
            <Route path="/" element={<PrivateOutlet />}>
              <Route path="home" element={<Home />} />
              <Route path="instrument" element={<Instrument />} />
              <Route path="role" element={<Role />} />
              <Route path="place" element={<Place />} />
              <Route path="church/:cityencoded" element={<Church />} />
              <Route path="state" element={<State />} />
              <Route path="city/:stateencoded" element={<City />} />
              <Route path="report" element={<Report />} />

              <Route path="/" exact element={<Home />} />
            </Route>
            <Route path="*" element={<NotFound />} />
          </Routes>
        </BrowserRouter>
  );
}

function PrivateOutlet(){
  return isAuthenticated() ? <Outlet /> : <Navigate to="/signin" />;
}
