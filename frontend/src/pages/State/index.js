import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { solid } from "@fortawesome/fontawesome-svg-core/import.macro";

import api from "../../services/api";
import { encode } from "../../services/handle";
import styles from "./styles.module.css";

import GoBack from "../../components/GoBack";

export default function State(){
    const navigate = useNavigate();
    const [ states, setStates ] = useState([]);

    const listState = () => {
        api().get('/state/list').then(response => {
            if(response.status == 200 && response.data.result && response.data.data)
                setStates(response.data.data);
        }).catch(err => {
            console.log(err);
        });
    }

    const handleClick = (code) => {
        navigate(`/city/${encode(code)}`);
    }

    useEffect(() => {
        listState();
    }, []);

    return (
        <div className={styles.content}>
            <GoBack goBackTo="/place" title="Estados" hideSync />
            <div>
                <div className={styles.itemsToCount}>
                    { states.map(state => (
                       <div key={state.uf} onClick={() => handleClick(state.code * 64000)}>
                            <div>{state.uf} - {state.name}</div>
                            <div><FontAwesomeIcon icon={solid("chevron-right")} /></div>
                       </div>
                    ))}
                </div>
            </div>
          
        </div>
    );
}