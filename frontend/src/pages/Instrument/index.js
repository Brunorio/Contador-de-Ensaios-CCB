import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import api from "../../services/api";
import styles from "./styles.module.css";
import violinImage from "../../assets/images/violin.svg";
import saxImage from "../../assets/images/sax.svg";
import trumpetImage from "../../assets/images/trumpet.svg";
import plusImage from "../../assets/images/plus.svg";

import GoBack from "../../components/GoBack";
import CounterCard from "../../components/CounterCard";

export default function Instrument(){
    const navigate = useNavigate(); 
    const [ instruments, setInstruments ] = useState([]);

    const listInstrument = () => {
        api().get('/instrument/list').then(response => {
            if(response.status == 200 && response.data.result && response.data.data)
                setInstruments(response.data.data);
        }).catch(err => {
            console.log(err);
        });
    }

    const handleChangeQuantity = (code, quantity) => {
        var index = instruments.findIndex(instrument => instrument.code == code);

        if(index < 0 || instruments[index].quantity + quantity < 0) return;

        api().put("instrument/insert", { code, quantity }).then(response => {
            if(response.status == 200 && response.data.result){
                var _instrument = [...instruments];
                _instrument[index].quantity += quantity;
                setInstruments(_instrument);
            } else navigate('/signin');

        }).catch(err => {
            console.log(err);
            navigate('/signin');
        });
    }

    const FloatBar = () => {
        return (
            <div className={styles.floatBar}>
                <div>
                    <a href="#strings"><img src={violinImage} /></a>
                </div>
                <div>
                    <a href="#wood"><img src={saxImage} /></a>
                </div>
                <div>
                    <a href="#brass"><img src={trumpetImage} /></a>
                </div>
                <div>
                    <a href="#others"><img src={plusImage} /></a>
                </div>
            </div>
        )
    }

    useEffect(() => {
        listInstrument();
    }, []);

    return (
        <div className={styles.content}>
            <GoBack backTo="/home" title="Instrumentos" />
            <div>
                <h2 id="strings">Cordas</h2>
                <div className={styles.itemsToCount}>
                    { instruments.filter(instrument => instrument.category == 'strings').map(instrument => (
                        <CounterCard 
                        handleValue={(quantity) => {
                            handleChangeQuantity(instrument.code, quantity);
                        }}
                        key={instrument.code} title={instrument.name} code={instrument.code} quantity={instrument.quantity}  />
                    ))}
                </div>
            </div>
            <div>
                <h2 id="wood">Madeiras</h2>
                <div className={styles.itemsToCount}>
                    { instruments.filter(instrument => instrument.category == 'wood').map(instrument => (
                        <CounterCard 
                        handleValue={(quantity) => {
                            handleChangeQuantity(instrument.code, quantity);
                        }}
                        key={instrument.code} title={instrument.name} code={instrument.code} quantity={instrument.quantity}  />
                    ))}
                </div>
            </div>
            <div>
                <h2 id="brass">Metais</h2>
                <div className={styles.itemsToCount}>
                    { instruments.filter(instrument => instrument.category == 'brass').map(instrument => (
                        <CounterCard 
                        handleValue={(quantity) => {
                            handleChangeQuantity(instrument.code, quantity);
                        }}
                        key={instrument.code} title={instrument.name} code={instrument.code} quantity={instrument.quantity}  />
                    ))}
                </div>
            </div>
            <div>
                <h2 id="others">Outros</h2>
                <div className={styles.itemsToCount}>
                    { instruments.filter(instrument => instrument.category == 'other').map(instrument => (
                        <CounterCard 
                        handleValue={(quantity) => {
                            handleChangeQuantity(instrument.code, quantity);
                        }}
                        key={instrument.code} title={instrument.name} code={instrument.code} quantity={instrument.quantity}  />
                    ))}
                </div>
            </div>
            <FloatBar />
        </div>
    );
}