import React, { useState, useEffect } from "react";
import { useParams, useNavigate } from 'react-router-dom';

import api from "../../services/api";
import styles from "./styles.module.css";
import { decode } from "../../services/handle";

import GoBack from "../../components/GoBack";
import CounterCard from "../../components/CounterCard";
import Loading from "../../components/Loading";

export default function Church(){
    const navigate = useNavigate();

    var { cityencoded } = useParams();
    const citycode = decode(cityencoded);

    const [ loading, setLoading ] = useState(true);
    const [ churchs, setChurchs ] = useState([]);
    const [ vowels, setVowels ] = useState([]);
    const [ city, setCity ] = useState({});

    const listChurch = () => {
        api().get(`/church/city/${citycode}`).then(response => {
            if(response.status == 200 && response.data.result && response.data.data){
                setChurchs(response.data.data.churchs || []);
                setCity(response.data.data.city || {});
            }
        }).catch(err => {
            console.log(err);
        }).finally(() => setLoading(false));
    }

    const handleChangeQuantity = (code, quantity) => {
        var index = churchs.findIndex(church => church.code == code);

        if(index < 0 || churchs[index].quantity + quantity < 0) return;

        api().put("city/insert", { city: churchs[index].city, quantity, church: churchs[index].code }).then(response => {
            if(response.status == 200 && response.data.result){
                var _churchs = [...churchs];
                _churchs[index].quantity += quantity;
                setChurchs(_churchs);
            } else navigate("/signin");

        }).catch(err => {
            console.log(err);
            navigate("/signin");
        });
    }

    useEffect(() => {
        listChurch();

        const intervalId = setInterval(() => {
            listChurch();
        }, 60000);
        
        return () => clearInterval(intervalId);
    }, []);

    useEffect(() => {
        var _vowels = new Set();
        churchs.forEach(church => _vowels.add(church.name.charAt(0)));
        _vowels = [..._vowels];
        _vowels.sort((a, b) => a.localeCompare(b));
        setVowels(_vowels);
    }, [churchs]);

    const FloatBar = () => {
        return (
            <div className={styles.floatBar + (vowels.length < 5 ? ' ' + styles.floatBarFlex : '')}>
                {vowels.map(vowel => (
                    <div key={vowel}>
                        <a href={`#${vowel.toLowerCase()}`}><div>{vowel}</div></a>
                    </div>
                ))}
                
            </div>
        )
    }

    const List = () => {
        return vowels.map(vowel => {
            var vowelLowerCase = vowel.toLowerCase();
            return(
                <div key={vowelLowerCase}>
                    <h2 id={vowelLowerCase}>{vowelLowerCase}</h2>
                    <div className={styles.itemsToCount}>
                        { churchs.filter(church => church.name.charAt(0).toLowerCase() == vowelLowerCase).map(church => {
                            return (
                                <CounterCard key={church.code}
                                handleValue={(quantity) => {
                                    handleChangeQuantity(church.code, quantity);
                                }}
                                title={church.name} code={church.code} quantity={church.quantity}  />
                            );
                        })}
                    </div>
                </div>
            );
        })
    }

    return (
        <div className={styles.content}>
            <Loading show={loading} />
            <GoBack backTo="/place" title={city.name} hideSync={churchs.length == 0} />

            {(churchs.length && <List />) || (
                <h3 style={{ textAlign: 'center' }}>Nenhuma comum encontrada</h3>
            )}
            
            <FloatBar />
        </div>
    );
}