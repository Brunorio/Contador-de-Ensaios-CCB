import React, { useEffect, useState } from "react";
import Button from 'react-bootstrap/Button';
import { listItem } from "../../helpers/GenericApi";
import "./styles.module.css";

import GoBack from "../../components/GoBack";
import { getStorageItem } from "../../services/storage";

export default function Report(){
    const [ general, setGeneral ] = useState([]);
    const [ cities, setCities ] = useState([]);
    const [ instruments, setInstruments ] = useState([]);
    const [ showPrintButton, setShowPrintButton ] = useState(true);
    const reportConf = getStorageItem("@reportConf") || {};

    useEffect(() => {
        
        listItem('role/list').then(results => {
            const resultFiltered = results.filter(result => result.quantity > 0);
            setGeneral(resultFiltered);
            listItem('instrument/list').then(resultsInstrument => {
                var _instruments = resultsInstrument.filter(i => i.quantity > 0);
                _instruments.push({
                    code: 500,
                    name: 'Orgão',
                    quantity: 1
                })
                setInstruments(_instruments);
                var _general = resultFiltered;
                _general.push({
                    code: 500,
                    name: 'Músicos',
                    quantity: _instruments.reduce((p, c) => p + c.quantity, 0)
                });
                setGeneral(_general);
            });
        });

        listItem('city/list').then(results => {
            setCities(results.filter(data => data.quantity > 0).map(data => data.uf === 'SP' ? data.name : data.namestate));
        })

    }, []);

    const calculateDate = (date) => {
        try {
            return new Date(date + 'T00:00').toLocaleDateString('pt-br', 
            { day: 'numeric', month: 'long', year: 'numeric' });
        } catch(error){
            console.log(error);
            return "";
        }
    }

    const handlePrintButton = () => {
        setShowPrintButton(false);

        setTimeout(() => {
            window.print();
        }, 500);
        
        setTimeout(() => {
            setShowPrintButton(true);
        }, 6000);
    }

    const buildHtmlInstruments = () => {
        var size = instruments.length;
        var divider = Math.trunc(size / 4);
        var rest = size % 4;
        var columns = {
            a: [],
            b: [],
            c: [],
            d: []
        };

        var accumulator = divider + (rest > 0 ? 1 : 0);
        var initial = 0;

        ['a', 'b', 'c', 'd'].forEach(letter => {
            columns[letter] = instruments.slice(initial, accumulator);
            
            initial = accumulator;
            rest--;
            accumulator += divider + (rest > 0 ? 1 : 0);
        });

        return columns['a'].map((row, index) => {
            return (
                <tr key={row.code}>
                    <td>{columns['a'][index]?.name || ''}</td>
                    <td>{columns['a'][index]?.quantity || ''}</td>
                    <td>{columns['b'][index]?.name || ''}</td>
                    <td>{columns['b'][index]?.quantity || ''}</td>
                    <td>{columns['c'][index]?.name || ''}</td>
                    <td>{columns['c'][index]?.quantity || ''}</td>
                    <td>{columns['d'][index]?.name || ''}</td>
                    <td>{columns['d'][index]?.quantity || ''}</td>
                </tr>
            )
        })
        
    }

    const calculatePercent = () => {
        var strings = instruments.filter(data => data.category == 'strings').reduce((p, c) => p + c.quantity, 0);
        var wood = instruments.filter(data => data.category == 'wood').reduce((p, c) => p + c.quantity, 0);
        var brass = instruments.filter(data => data.category == 'brass').reduce((p, c) => p + c.quantity, 0);

        var total = wood + brass + strings;
        var percentStrings = parseInt(strings / total * 100);
        var percentWood = parseInt(wood / total * 100);
        var percentBrass = parseInt(brass / total * 100);

        var difference = percentBrass + percentStrings + percentWood;

        if(difference < 100)
            percentStrings += 100 - difference;

        return (
            <>
                {percentStrings}% de Cordas<br />
                {percentWood}% de Madeiras<br />
                {percentBrass}% de Metais<br />
            </>
        )
    }

    return (
        <div style={{ padding: '20px', marginTop: showPrintButton ? '80px' : 0 }}>
            
            {showPrintButton ? 
                <>
                    <GoBack backTo="/home" title="Relatório" />
                    <Button style={{ color: '#fff', marginBottom: 10, marginLeft: 5 }} variant="info" onClick={handlePrintButton}>
                        Imprimir
                    </Button>
                </>
             : ""}
            
            <table>
                <tr style={{ textAlign: 'center' }}>
                    <td>
                        CONGREGAÇÃO CRISTÃO NO BRASIL<br />
                        RELATÓRIO {reportConf.type ? `DO ${String(reportConf.type).toUpperCase()}` : "" } 
                        {reportConf.city ? ` DE ${String(reportConf.city).toUpperCase()}` : "" }<br />
                        {reportConf.date ? calculateDate(reportConf.date) : "" }
                    </td>
                </tr>
                <tr>
                    <table>
                        <tr>
                            <td><strong>INÍCIO:</strong> {reportConf.start ? `${reportConf.start} HORAS` : "-" }  </td>
                            <td><strong>PREVISÃO DE TÉRMINO:</strong> {reportConf.end ? `${reportConf.end} HORAS` : "-" }</td>
                        </tr>
                        <tr>
                            <td><strong>PRESIDÊNCIA:</strong> {reportConf.master || "-" }</td>
                            <td><strong>PALAVRA: </strong> {reportConf.holy || "-" }</td>
                        </tr>
                        <tr>
                            <td colSpan={2}><strong>ENCARREGADO: </strong> {reportConf.conductor || "-" }</td>
                        </tr>
                    </table>
                </tr>
                <tr>
                    <table>
                        <tr>
                            <td style={{ textAlign: 'center' }}><strong>INSTRUMENTOS</strong></td>
                        </tr>                        
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <th style={{ width: '19%' }}>NOME</th>
                                        <th style={{ width: '6%' }}>QTDE.</th>
                                        <th style={{ width: '19%' }}>NOME</th>
                                        <th style={{ width: '6%' }}>QTDE.</th>
                                        <th style={{ width: '19%' }}>NOME</th>
                                        <th style={{ width: '6%' }}>QTDE.</th>
                                        <th style={{ width: '19%' }}>NOME</th>
                                        <th style={{ width: '6%' }}>QTDE.</th>
                                    </tr>
                                    
                                    {buildHtmlInstruments()}
                                   
                                    <tr>
                                        <td style={{ textAlign: 'right' }} colSpan={8}>
                                            <strong>TOTAL DE MÚSICOS: </strong>
                                            {instruments.reduce((p, c) => p + c.quantity, 0)}
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            
                        </tr>
                    </table>
                </tr>
                <tr>
                    <table>
                        <tr>
                            <td style={{ textAlign: 'center' }}><strong>RESUMO GERAL DOS PARTICIPANTES</strong></td>
                        </tr>                        
                        <tr>
                            <td>
                                <table>
                                    
                                    <tr>
                                        <th style={{ width: '6%' }}>QTDE.</th>
                                        <th>NOME</th>
                                        <th style={{ width: '50%' }}>OBSERVAÇÃO</th>
                                    </tr>
                                    {general.length ? (
                                        <tr key={general[0].code}>
                                            <td>{general[0].quantity}</td>
                                            <td>{general[0].name}</td>
                                            <td style={{ textAlign: 'center' }} rowSpan={general.length}>
                                                {calculatePercent()}
                                            </td>
                                        </tr>
                                    ) : null}
                                    
                                    {general.slice(1).map(data => (
                                        <tr key={data.code}>
                                            <td>{data.quantity}</td>
                                            <td>{data.name}</td>
                                        </tr>
                                    ))}
                                 
                                    <tr>
                                        <td style={{ textAlign: 'right' }} colSpan={3}>
                                            <strong>TOTAL GERAL: </strong>
                                            {general.reduce((p, c) => p + c.quantity, 0)}
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            
                        </tr>
                    </table>
                </tr>
                <tr>
                    <table>
                        <tr>
                            <td style={{ textAlign: 'center' }}><strong>CIDADES PARTICIPANTES</strong></td>
                        </tr>                        
                        <tr>
                            <td>
                                <table>
                                    
                                    <tr>
                                        <td>
                                            {cities.join(', ')}
                                        </td>
                                    </tr>
                                 
                                    <tr>
                                        <td style={{ textAlign: 'right' }} colSpan={4}><strong>TOTAL DE CIDADES: </strong>
                                        {cities.length}
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            
                        </tr>
                    </table>
                </tr>
            </table>
        </div>
    )
}