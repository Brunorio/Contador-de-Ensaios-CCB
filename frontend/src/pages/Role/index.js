import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import api from "../../services/api";
import styles from "./styles.module.css";


import GoBack from "../../components/GoBack";
import CounterCard from "../../components/CounterCard";

export default function Role(){
    const navigate = useNavigate();

    const [ roles, setRoles ] = useState([]);

    const listRole = () => {
        api().get('/role/list').then(response => {
            if(response.status == 200 && response.data.result && response.data.data)
                setRoles(response.data.data);
        }).catch(err => {
            console.log(err);
        });
    }

    const handleChangeQuantity = (code, quantity) => {
        var index = roles.findIndex(role => role.code == code);

        if(index < 0 || roles[index].quantity + quantity < 0) return;

        api().put("role/insert", { code, quantity }).then(response => {
            if(response.status == 200 && response.data.result){
                var _roles = [...roles];
                _roles[index].quantity += quantity;
                setRoles(_roles);
            } else navigate("/signin");

        }).catch(err => {
            console.log(err);
            navigate("/signin");
        });
    }

    useEffect(() => {
        listRole();
    }, []);

    return (
        <div className={styles.content}>
            <GoBack title="Geral" />
            <div>
                <div className={styles.itemsToCount}>
                    { roles.map(role => (
                        <CounterCard 
                        handleValue={(quantity) => {
                            handleChangeQuantity(role.code, quantity);
                        }}
                        key={role.code} title={role.name} code={role.code} quantity={role.quantity}  />
                    ))}
                </div>
            </div>
          
        </div>
    );
}