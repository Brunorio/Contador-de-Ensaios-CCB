import React, { useState, useEffect } from 'react';
import { useNavigate } from "react-router-dom";
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Form from 'react-bootstrap/Form';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { solid } from "@fortawesome/fontawesome-svg-core/import.macro";

import api from "../../services/api";
import { getAuthData, setAuthData } from "../../services/auth";
import styles from "./styles.module.css";
import { getStorageItem, setStorageItem } from '../../services/storage';

export default function Home() {
    const [ showModal, setShowModal ] = useState(false);
    const [ showModalReport, setShowModalReport ] = useState(false);
    const [ event, setEvent ] = useState(getAuthData().event);
    const [ reportConf, setReportConf ] = useState({type: 'Ensaio Regional'});

    var navigate = useNavigate();


    const handleReportConf = (index, value) => {
        var copy = {...reportConf} || {};
        copy[index] = value;
        setReportConf(copy);
    }

    const handleClose = () => {
        setShowModal(false);
        setShowModalReport(false);
    }

    const handleConfirm = () => {
        api().get('/event/finish').then(response => {
            if(response.status == 200 && response.data.result){
                var _event = event;
                _event.finished = true;
                setEvent(_event);
                setAuthData({
                    ...getAuthData(),
                    event: _event
                });
            }
            
        }).catch(err => console.log(err)).finally(handleClose);
    }

    useEffect(() => {
        var data = getStorageItem("@reportConf");
        if(data) setReportConf(data);

    }, []);

    const SignOut = () => {
        return (
            <div className={styles.signOut} onClick={() => navigate("/signin")}>
                <FontAwesomeIcon size="2xl" icon={solid("power-off")} />
                <span>Sair</span>
            </div>
        )
    }

    return (
        <div className={styles.content}>
            <SignOut />
            <div><h1>{event.name}</h1></div>
            <div className={styles.options}>
                <Button variant="success" onClick={() => navigate("/instrument") }>
                    <FontAwesomeIcon size="xl" icon={solid("guitar")} />
                    <span className={styles.optionTitle}>Instrumento</span>
                </Button>
                <Button variant="info" onClick={() => navigate("/place") }>
                    <FontAwesomeIcon size="xl" icon={solid("location-dot")} />
                    <span className={styles.optionTitle}>Localidade</span>
                </Button>
                <Button variant="warning" onClick={() => navigate("/role") }>
                    <FontAwesomeIcon size="xl" icon={solid("users")} />
                    <span className={styles.optionTitle}>Geral</span>
                </Button>
                <Button variant="outline-secondary" onClick={() => setShowModalReport(true) }>
                    <FontAwesomeIcon size="xl" icon={solid("circle-info")} />
                    <span className={styles.optionTitle}>Gerar Relatório</span>
                </Button>
                
            </div>
            <Modal show={showModal} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Finalizar contagem</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>Você tem certeza que deseja finalizar esta contagem?</h4>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Cancelar
                    </Button>
                    <Button variant="primary" onClick={() => {
                        handleConfirm();
                    }}>
                        Confirmar
                    </Button>
                </Modal.Footer>
            </Modal>

            <Modal show={showModalReport} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Dados do Relátorio</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form.Group className="mb-3" controlId="formBasicQuantity">
                        <Form.Label>Cidade</Form.Label>
                        <Form.Control size="lg" value={reportConf.city || ""} onChange={e => {
                            handleReportConf('city', e.target.value);
                        }} autoFocus type="text" placeholder="Cidade" 
                        />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicQuantity">
                        <Form.Label>Tipo</Form.Label>
                        <Form.Select size="lg" value={reportConf.type || ""} onChange={e => {
                            handleReportConf('type', e.target.value);
                        }}>
                            <option>Ensaio Regional</option>
                            <option>Ensaio Local</option>
                        </Form.Select>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicQuantity">
                        <Form.Label>Data</Form.Label>
                        <Form.Control size="lg" value={reportConf.date || ""} onChange={e => {
                            handleReportConf('date', e.target.value);
                        }}  type="date" placeholder="Data" 
                        />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicQuantity">
                        <Form.Label>Início</Form.Label>
                        <Form.Control size="lg" value={reportConf.start || ""} onChange={e => {
                            handleReportConf('start', e.target.value);
                        }} type="time" placeholder="Início" 
                        />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicQuantity">
                        <Form.Label>Previsão de Término</Form.Label>
                        <Form.Control size="lg" value={reportConf.end || ""} onChange={e => {
                            handleReportConf('end', e.target.value);
                        }}  type="time" placeholder="Previsão de Término" 
                        />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicQuantity">
                        <Form.Label>Presidência</Form.Label>
                        <Form.Control size="lg" value={reportConf.master || ""} onChange={e => {
                            handleReportConf('master', e.target.value);
                        }}  type="text" placeholder="Presidência" 
                        />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicQuantity">
                        <Form.Label>Palavra</Form.Label>
                        <Form.Control size="lg" value={reportConf.holy || ""} onChange={e => {
                            handleReportConf('holy', e.target.value);
                        }}  type="text" placeholder="Palavra" 
                        />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicQuantity">
                        <Form.Label>Encarregado</Form.Label>
                        <Form.Control size="lg" value={reportConf.conductor || null} onChange={e => {
                            handleReportConf('conductor', e.target.value);
                        }}  type="text" placeholder="Encarregado" 
                        />
                    </Form.Group>
                    
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Cancelar
                    </Button>
                    <Button variant="primary" onClick={() => {
                        setStorageItem('@reportConf', reportConf);
                        navigate('/report');
                    }}>
                        Confirmar
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
    );
}
