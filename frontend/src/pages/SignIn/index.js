import React, { useEffect, useState } from 'react';
import { useNavigate } from "react-router-dom";
import Button from 'react-bootstrap/Button';

import Form from 'react-bootstrap/Form';

import NewEvent from '../../components/NewEvent';
import AlertMessage from '../../components/AlertMessage';
import Loading from '../../components/Loading';
import api from '../../services/api';
import { setAuthData, removeAuthData } from '../../services/auth';

import styles from './styles.module.css';

export default function SignIn() {
    var navigate = useNavigate();
   
    const [ accessKey, setAccessKey ] = useState("");
    const [showModal, setShowModal] = useState(false);
    const [ showAlert, setShowAlert ] = useState(false);
    const [ loading, setLoading ] = useState(false);
    const [ alertMessage, setAlertMessage ] = useState("");

    const handleSignIn = () => {
        if(!accessKey) {
            setAlertMessage("Informe a chave de acesso do Contador de Ensaio para continuar!");
            return setShowAlert(true);
        }

        setLoading(true);

        api().post("/signin", { accessKey }).then(response => {
            if(response && response.status === 200 && response.data.result && response.data.data){
                setAuthData(response.data.data);
                return navigate('/home');
            } else {
                setAlertMessage(response.data.message);
                setShowAlert(true);
                setLoading(false)
            }
        }).catch(err => {
            setAlertMessage("Não foi possível realizar a busca pelo Contador de Ensaio");
            setShowAlert(true);
            setLoading(false);
        });
    }

    useEffect(() => {
        removeAuthData();
    }, []);

    return (
        <div className={styles.content}>
            <Loading show={loading} />
            <Form>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Chave de Acesso</Form.Label>
                    <Form.Control size="lg" maxLength={10} value={accessKey || ""} onChange={e => {
                        setAccessKey(e.target.value || "");
                    }} autoFocus type="text" placeholder="Informe a chave de acesso do ensaio" 
                    style={{ textTransform: "uppercase" }} />
                </Form.Group>
                <Button variant="primary" type="button" onClick={handleSignIn} style={{ marginBottom: 30 }}>
                    Acessar
                </Button>
                <AlertMessage show={showAlert} onClose={() => setShowAlert(false)} variant="danger" message={alertMessage}  />
                
            </Form>
            <div>
                <Button variant="link" type="button" onClick={() => setShowModal(true)} style={{ marginBottom: 30 }}>
                    Adicionar Nova Contagem de Ensaio
                </Button>
            </div>
            <NewEvent showModal={showModal} handleClose={() => setShowModal(false)} />
        </div>
    )
}