import React, { useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { solid } from "@fortawesome/fontawesome-svg-core/import.macro";
import Form from 'react-bootstrap/Form';

import api from "../../services/api";
import { decode } from "../../services/handle";
import styles from "./styles.module.css";

import GoBack from "../../components/GoBack";

export default function City(){
    const navigate = useNavigate();
    var listCitiesSelected = [];

    const [ cities, setCities ] = useState([]);
    const [ search, setSearch ] = useState('');
    const [ state, setState ] = useState({});

    var { stateencoded } = useParams();
    const statecode = (decode(stateencoded) / 64000);

    const listCities = () => {
        api().get(`/city/state/${statecode}`).then(response => {
            if(response.status == 200 && response.data.result && response.data.data){
                setCities(response.data.data.cities);
                setState(response.data.data.state);
            }
        }).catch(err => {
            console.log(err);
        });
    }

    const handleClick = (code) => {
        if(!listCitiesSelected.find(city => city == code)){
            api().put('city/include', { city: code }).then(response => {
                if(response.status == 200 && response.data.result){
                    var _cities = [...cities];
                    var index = cities.findIndex(city => city.code == code);
                    if(index >= 0){
                        _cities[index].isenabled = true;
                        setCities(_cities);
                        listCitiesSelected.push(code);
                    }
                } else navigate("/signin");
            }).catch(err => {
                console.log(err);
                navigate("/signin");
            });
        }
    }

    const convertToSlug = (text) => {
        const a = 'àáäâãèéëêìíïîòóöôùúüûñçßÿœæŕśńṕẃǵǹḿǘẍźḧ·/_,:;'
        const b = 'aaaaaeeeeiiiioooouuuuncsyoarsnpwgnmuxzh------'
        const p = new RegExp(a.split('').join('|'), 'g')
        return text.toString().toLowerCase().trim()
          .replace(p, c => b.charAt(a.indexOf(c))) // Replace special chars
          .replace(/&/g, '-and-') // Replace & with 'and'
          .replace(/[\s\W-]+/g, '-') // Replace spaces, non-word characters and dashes with a single dash (-)
      }

    useEffect(() => {
        listCitiesSelected = [];
        listCities();
    }, []);

    return (
        <div className={styles.content}>
            <GoBack goBackTo="/state" title={state.name} hideSync />
            <div>
                <Form.Group className="mb-3" controlId="formBasicQuantity">
                    <Form.Label>Nome da Cidade</Form.Label>
                    <Form.Control size="lg" value={search || ""} onChange={e => {
                        setSearch(e.target.value || 0);
                    }} autoFocus type="text" placeholder="Nome da Cidade" 
                    />
                </Form.Group>
            </div>
            <hr />
            {search.length > 3 && (
                <div>
                    <div className={styles.itemsToCount}>
                        { cities.filter(city => convertToSlug(city.name).toLowerCase().search(convertToSlug(search).toLowerCase()) >= 0).map(city => (
                        <div className={city.isenabled ? styles.nocursorpointer : ""} key={city.code} 
                            onClick={!city.isenabled ? () => handleClick(city.code) : () => {}}
                        >
                                <div>{city.name}</div>
                                {city.isenabled ? (
                                    <div><FontAwesomeIcon size="xl" color={'green'} icon={solid("check")} /></div>
                                ) : (
                                    <div><FontAwesomeIcon size="xl" icon={solid("plus")} /></div>
                                )}
                        </div>
                        ))}
                    </div>
                </div>
            )}
        </div>
    );
}