import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { solid } from "@fortawesome/fontawesome-svg-core/import.macro";

import api from "../../services/api";
import styles from "./styles.module.css";
import { getAuthData } from "../../services/auth";

import GoBack from "../../components/GoBack";
import CounterCard from "../../components/CounterCard";
import CounterChurchCard from "../../components/CounterChurchCard";
import { encode } from "../../services/handle";

export default function Place(){
    const navigate = useNavigate();

    const [ vowels, setVowels ] = useState([]);
    const [ cities, setCities ] = useState([]);
    const event = getAuthData().event;
    const citiesToUseChurch = event.citiesToUseChurch || [];

    const listCity = () => {
        api().get('/city/list').then(response => {
            if(response.status == 200 && response.data.result && response.data.data)
                setCities(response.data.data);
        }).catch(err => {
            console.log(err);
        });
    }

    const handleChangeQuantity = (code, quantity) => {
        var index = cities.findIndex(city => city.code == code);

        if(index < 0 || cities[index].quantity + quantity < 0) return;

        api().put("city/insert", { city: code, quantity }).then(response => {
            if(response.status == 200 && response.data.result){
                var _cities = [...cities];
                _cities[index].quantity += quantity;
                setCities(_cities);
            } else navigate("/signin");

        }).catch(err => {
            console.log(err);
            navigate("/signin");
        });
    }

    useEffect(() => {
        listCity();

        const intervalId = setInterval(() => {
            listCity();
        }, 60000);
        
        return () => clearInterval(intervalId);
    }, []);

    useEffect(() => {
        var _vowels = new Set();
        cities.forEach(city => _vowels.add(city.name.charAt(0)));
        _vowels = [..._vowels];
        _vowels.sort((a, b) => a.localeCompare(b));
        setVowels(_vowels);
    }, [cities]);

    const FloatBar = () => {
        return (
            <div className={styles.floatBar + (vowels.length < 5 ? ' ' + styles.floatBarFlex : '')}>
                {vowels.map(vowel => (
                    <div key={vowel}>
                        <a href={`#${vowel.toLowerCase()}`}><div>{vowel}</div></a>
                    </div>
                ))}
                
            </div>
        )
    }

    const AddButton = () => {
        return (
            <div onClick={() => navigate("/state")} className={styles.addButton}>
                <div><FontAwesomeIcon size="lg" icon={solid("plus")} /></div>
            </div>
        )
    }

    return (
        <div className={styles.content}>
            <GoBack backTo="/home" title="Localidades" />
            <div>
                {vowels.map(vowel => {
                    var vowelLowerCase = vowel.toLowerCase();
                    return(
                        <div key={vowelLowerCase}>
                            <h2 id={vowelLowerCase}>{vowelLowerCase}</h2>
                            <div className={styles.itemsToCount}>
                                { cities.filter(city => city.name.charAt(0).toLowerCase() == vowelLowerCase).map(city => {
                                    if(citiesToUseChurch.length && citiesToUseChurch.find(citycode => citycode == city.code))
                                        return(
                                            <CounterChurchCard code={encode(city.code)} key={city.code} title={city.namestate} quantity={city.quantity} />
                                        );
    
                                    return (
                                        <CounterCard key={city.code}
                                        handleValue={(quantity) => {
                                            handleChangeQuantity(city.code, quantity);
                                        }}
                                        title={city.namestate} code={city.code} quantity={city.quantity}  />
                                    );
                                })}
                            </div>
                        </div>
                    );
                })}
            </div>
            {!event.finished && <AddButton />}
            <FloatBar />
        </div>
    );
}